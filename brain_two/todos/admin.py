from django.contrib import admin
from todos.models import TodoList
from todos.models import Todoitem

# Register your models here.

@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
      list_display=[
            "name",
            "id"
      ]

@admin.register(Todoitem)
class TodoItemAdmin(admin.ModelAdmin):
      list_display=[
            "task",
            "due_date",
            "is_completed",
            "list"
      ]

