from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList

# Create your views here.
def todo_list_list(request):
    todo_lists=TodoList.objects.all()
    context={"todo_lists": todo_lists}
    return render(request, "todos/todo_list_list.html", context)

def todo_list_detail(request, id):
    todo_list=get_object_or_404(TodoList, id=id)
    context={"todo_list":todo_list}
    return render(request, "todos/todo_list_detail.html", context)

def todo_list_create(request):
    return render(request, "todos/create_todo_list.html")

def save_todo_list(request):
    task_name=request.POST.get("todoListName")
    todo_list=TodoList.objects.create(name=task_name)
    return redirect("/todos/" + str(todo_list.id))

