from django.urls import path
from todos.views import todo_list_list 
from todos.views import todo_list_detail
from todos.views import create

urlpatterns=[
    path("todos/<int:id>/", todo_list_list),
    path("todo_list_detail/<int:id>", todo_list_detail)
]